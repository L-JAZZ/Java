package com.example.drugstore.demo;

import org.springframework.stereotype.Component;

@Component("drug")
public class Drug {
    String name;
    int permission;
    double price;

    public Drug() {}

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
