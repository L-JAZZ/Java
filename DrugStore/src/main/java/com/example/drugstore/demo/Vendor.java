package com.example.drugstore.demo;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component("vendor")
public class Vendor {

    String name = "Euro Med";
    List<Drug> drugs = new ArrayList<>();

    // gives us exact drug from list
    public Drug searchDrug(String drugName,List<Drug> drugs){
        for (Drug drug : drugs) {
            if (drug.name.equals(drugName)) {
                return drug;
            }
        }
        return null;
    }

    public void sellDrug(Customer customer, String drugName) {

        Drug drug = searchDrug(drugName, drugs);

        if (drug != null) {
            if (customer.balance < drug.price) {
                System.out.println(customer.name + " you don't have enough money");

            } else if (checkPermission(customer, drug)) {

                System.out.println("========= Thanks for purchase! ========= \n You bought " + drug.name);

                customer.setBalance(customer.balance - drug.price);

                System.out.println("your balance is: " + customer.balance);

            } else System.out.println("you don't have enough permission");

        }else System.out.println("there is no such medicine like " + drugName );
    }

    public boolean checkPermission(Customer customer, Drug drug){ // to check if medicine can be purchased by customer
        if(customer.permission < drug.permission){
            return false;
        }else return true;
    }
    /////---------------------------------------- setters and getters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addDrug(Drug drug){
        drugs.add(drug);
    }

    public void removeDrug(Drug drug){
        drugs.remove(drug);
    }
}
