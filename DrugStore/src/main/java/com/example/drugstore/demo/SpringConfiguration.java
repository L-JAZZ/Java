package com.example.drugstore.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan("com.example.drugstore")
public class SpringConfiguration {

    @Bean("customer")
    public Customer customer(){
        Customer customer = new Customer();
        customer.setName("Johnny");
        customer.setBalance(5566.32);
        customer.setPermission(1);
        return customer;
    }

    @Bean("drug")
    public Drug drug(){
        Drug drug = new Drug();
        drug.setName("morphine");
        drug.setPrice(600.99);
        drug.setPermission(3);
        return drug;
    }

    @Bean("vendor")
    public Vendor vendor(){
        Vendor vendor = new Vendor();
        vendor.setName("Emir Med");
        return vendor;
    }



    @Bean("database")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSource.setUsername("postgres");
        dataSource.setPassword("1");
        return dataSource;
    }

    @Bean("template")
    public JdbcTemplate jdbcTemplate(){
        return new JdbcTemplate(dataSource());
    }
}
