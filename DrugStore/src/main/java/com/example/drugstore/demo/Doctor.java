package com.example.drugstore.demo;

import org.springframework.stereotype.Component;

@Component("doc")
public class Doctor {

    private static Doctor instance;

    public static Doctor getInstance(){
        if(instance == null){
            return new Doctor();
        }else return instance;
    }

    public void setPermission(Customer customer,int newPermission){
        customer.permission = newPermission;
        System.out.println(customer.name + "Now has permission " + customer.permission);
    }
}
