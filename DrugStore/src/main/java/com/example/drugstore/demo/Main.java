package com.example.drugstore.demo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfiguration.class);

        Doctor doctor = Doctor.getInstance();

        Vendor vendor = context.getBean("vendor",Vendor.class);
        Customer customer = context.getBean("customer",Customer.class);
        Drug drug1 = context.getBean("drug",Drug.class);

        vendor.addDrug(drug1);
        vendor.sellDrug(customer, "aspirine");
        vendor.sellDrug(customer, "morphine");
        doctor.setPermission(customer, 3);
        vendor.sellDrug(customer, "morphine");

        //Database connection
        DriverManagerDataSource dataSource = context.getBean("database", DriverManagerDataSource.class);
        //idk why there is an error, i've set the correct url for database
        final Connection connection = DriverManager.getConnection(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
    }
}
